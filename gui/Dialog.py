from PyQt5.QtWidgets import QDialog, QLabel, QVBoxLayout, QDialogButtonBox
from PyQt5.QtCore import Qt

class ChooseDialog(QDialog):

    def __init__(self, parent=None):
        super(ChooseDialog, self).__init__(parent)
        self.setWindowTitle('Game Over')
        self.setFixedSize(320,80)
        self.setWindowModality(Qt.ApplicationModal)
        self.__initComponents()
        self.setLayout(self.__layout)
        self.show()

    def __initComponents(self):
        self.__infotextlabel = QLabel()
        self.__buttons = QDialogButtonBox.Ok | QDialogButtonBox.Cancel
        self.__buttonbox = QDialogButtonBox(self.__buttons)
        self.__buttonbox.accepted.connect(self.accept)
        self.__buttonbox.rejected.connect(self.reject)
        self.__layout = QVBoxLayout()
        self.__layout.addWidget(self.__infotextlabel)
        self.__layout.addWidget(self.__buttonbox)

    def setInfoMessage(self, message):
        self.__infotextlabel.setText(message)

class InfoDialog(QDialog):

    def __init__(self, parent=None):
        super(InfoDialog, self).__init__(parent)
        self.setWindowTitle('Information')
        self.setFixedSize(400,80)
        self.setWindowModality(Qt.ApplicationModal)
        self.__initComponents()
        self.setLayout(self.__layout)
        self.show()

    def __initComponents(self):
        self.__infotextlabel = QLabel()
        self.__buttons = QDialogButtonBox.Ok
        self.__buttonbox = QDialogButtonBox(self.__buttons)
        self.__buttonbox.accepted.connect(self.accept)
        self.__layout = QVBoxLayout()
        self.__layout.addWidget(self.__infotextlabel)
        self.__layout.addWidget(self.__buttonbox)

    def setInfoMessage(self, message):
        self.__infotextlabel.setText(message)