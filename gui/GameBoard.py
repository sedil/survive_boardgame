from PyQt5.QtWidgets import QWidget
from PyQt5.QtGui import QPainter, QColor

class GameBoard(QWidget):

    def __init__(self, mainframe, board, parent=None):
        super(GameBoard, self).__init__(parent)
        self.mainframe = mainframe
        self.board = board
        self.mainframe.updateStatusPanel()

        self.__gameinit = True
        self.__rectsize = 80
        self.__mousecoordinate = [-1,-1]
        self.setMouseTracking(True)
        self.setFixedSize(640, 640)
        self.__initColors()
        self.setGeometry(100, 100, 640, 640)
        self.setFixedSize(640, 640)
        self.show()

    def __initColors(self):
        self.lightgray = QColor(190, 190, 190)
        self.darkgray = QColor(105, 105, 105)
        self.white = QColor(255, 255, 255)
        self.black = QColor(0, 0, 0)
        self.red = QColor(255, 0, 0)
        self.blue = QColor(0, 0, 255)
        self.green = QColor(0, 255, 0)
        self.yellow = QColor(255, 255, 0)
        self.violet = QColor(102, 0, 102)

    def __clear(self, painter):
        painter.setPen(self.lightgray)
        painter.setBrush(self.lightgray)
        painter.drawRect(0, 0, 640, 640)

    def mousePressEvent(self, mouseevent):
        direction = self.__direction(mouseevent.x(), mouseevent.y())
        self.__mousecoordinate[0] = int(mouseevent.x() / self.__rectsize)
        self.__mousecoordinate[1] = int(mouseevent.y() / self.__rectsize)
        if self.board.roundloop(self.__mousecoordinate[0],self.__mousecoordinate[1], direction):
            self.update()
            self.mainframe.updateStatusPanel()

    def __direction(self, px, py):
        cell_x = int(px / self.__rectsize)
        cell_y = int(py / self.__rectsize)
        px -= cell_x * self.__rectsize
        py -= cell_y * self.__rectsize

        if px >= 20 and px <= 60 and py >= 0 and py <= 20:
            return (cell_x,cell_y - 1)
        elif px >= 60 and py >= 20 and py <= 60:
            return (cell_x + 1,cell_y)
        elif px <= 20 and py >= 20 and py <= 60:
            return (cell_x - 1,cell_y)
        elif px >= 20 and px <= 60 and py >= 60 and py <= 80:
            return (cell_x,cell_y + 1)
        elif px >= 20 and px <= 60 and py >= 20 and py <= 60:
            return (cell_x,cell_y)

    def paintEvent(self, event):
        painter = QPainter()
        painter.begin(self)
        self.__clear(painter)
        self.__drawChessPattern(painter)
        self.__drawStones(painter)
        painter.end()

    def __drawChessPattern(self, painter):
        painter.setPen(self.lightgray)
        painter.setBrush(self.lightgray)
        painter.drawRect(0, 0, 640, 640)

        painter.setBrush(self.darkgray)
        for i in range(8):
            for j in range(8):
                if i % 2 == 0 and j % 2 == 0:
                    painter.drawRect(i * self.__rectsize, j * self.__rectsize, self.__rectsize, self.__rectsize)
                elif not i % 2 == 0 and not j % 2 == 0:
                    painter.drawRect(i * self.__rectsize, j * self.__rectsize, self.__rectsize, self.__rectsize)

    def __drawStones(self, painter):
        painter.setPen(self.white)

        whitestones = self.board.getWhiteStoneList()
        for elem in whitestones:
            x = elem.getPosition()[0] * self.__rectsize
            y = elem.getPosition()[1] * self.__rectsize
            painter.setBrush(self.white)
            painter.drawEllipse(10 + x, 10 + y, 60, 60)

            if elem == self.board.getActualStone():
                painter.setBrush(self.violet)
                painter.drawRect(65 + x, 65 + y, 15, 15)

        painter.setPen(self.black)
        blackstones = self.board.getBlackStoneList()
        for elem in blackstones:
            x = elem.getPosition()[0] * self.__rectsize
            y = elem.getPosition()[1] * self.__rectsize
            painter.setBrush(self.black)
            painter.drawEllipse(10 + x, 10 + y, 60, 60)

            if elem == self.board.getActualStone():
                painter.setBrush(self.violet)
                painter.drawRect(65 + x, 65 + y, 15, 15)

        redstone = self.board.getColorStoneList()[0]
        yellowstone = self.board.getColorStoneList()[1]
        greenstone = self.board.getColorStoneList()[2]
        bluestone = self.board.getColorStoneList()[3]

        painter.setPen(self.red)
        painter.setBrush(self.red)
        painter.drawEllipse(15 + redstone.getPosition()[0] * self.__rectsize, 15 + redstone.getPosition()[1] * self.__rectsize, 50, 50)

        painter.setPen(self.yellow)
        painter.setBrush(self.yellow)
        painter.drawEllipse(15 + yellowstone.getPosition()[0] * self.__rectsize, 15 + yellowstone.getPosition()[1] * self.__rectsize, 50, 50)

        painter.setPen(self.green)
        painter.setBrush(self.green)
        painter.drawEllipse(15 + greenstone.getPosition()[0] * self.__rectsize, 15 + greenstone.getPosition()[1] * self.__rectsize, 50, 50)

        painter.setPen(self.blue)
        painter.setBrush(self.blue)
        painter.drawEllipse(15 + bluestone.getPosition()[0] * self.__rectsize, 15 + bluestone.getPosition()[1] * self.__rectsize, 50, 50)

    def getClickedCoordinate(self):
        return self.__mousecoordinate