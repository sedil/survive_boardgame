from PyQt5.QtWidgets import QWidget, QLabel, QVBoxLayout, QHBoxLayout
from PyQt5.QtGui import QColor

class StatusPanel(QWidget):

    def __init__(self, parent=None):
        super(StatusPanel, self).__init__(parent)
        self.__initComponents()
        self.__initLayout()
        self.setFixedSize(640, 80)
        self.setLayout(self.__layout)
        self.show()

    def __initLayout(self):
        self.__dicelayout = QVBoxLayout()
        self.__dicelayout.addWidget(self.__dicestatus)
        self.__dicelayout.addWidget(self.__dicecounter)

        self.__stoneslayout = QVBoxLayout()
        self.__stoneslayout.addWidget(self.__playerstatus)
        self.__stoneslayout.addWidget(self.__whitestonecounter)
        self.__stoneslayout.addWidget(self.__blackstonecounter)

        self.__whitestatus = QHBoxLayout()
        self.__whitestatus.addWidget(self.__white_greenflag)
        self.__whitestatus.addWidget(self.__white_yellowflag)
        self.__whitestatus.addWidget(self.__white_redflag)
        self.__whitestatus.addWidget(self.__white_blueflag)

        self.__blackstatus = QHBoxLayout()
        self.__blackstatus.addWidget(self.__black_greenflag)
        self.__blackstatus.addWidget(self.__black_yellowflag)
        self.__blackstatus.addWidget(self.__black_redflag)
        self.__blackstatus.addWidget(self.__black_blueflag)

        self.__statuslabel = QVBoxLayout()
        self.__statuslabel.addWidget(self.__status)
        self.__statuslabel.addLayout(self.__whitestatus)
        self.__statuslabel.addLayout(self.__blackstatus)

        self.__layout = QHBoxLayout()
        self.__layout.addLayout(self.__dicelayout)
        self.__layout.addLayout(self.__stoneslayout)
        self.__layout.addLayout(self.__statuslabel)

    def __initComponents(self):
        self.red = QColor(255, 0, 0)
        self.blue = QColor(0, 0, 255)
        self.green = QColor(0, 255, 0)
        self.yellow = QColor(255, 255, 0)
        self.gray = QColor(190, 190, 190)

        self.__dicestatus = QLabel('Dice')
        self.__dicecounter = QLabel(str(''))
        self.__playerstatus = QLabel(str('Stones'))
        self.__whitestonecounter = QLabel(str(''))
        self.__whitestonecounter.setAutoFillBackground(True)
        self.__blackstonecounter = QLabel(str(''))
        self.__blackstonecounter.setAutoFillBackground(True)

        self.__status = QLabel('STATUS')
        self.__white_greenflag = QLabel('   ')
        self.__white_greenflag.setAutoFillBackground(True)
        self.__white_yellowflag = QLabel('   ')
        self.__white_yellowflag.setAutoFillBackground(True)
        self.__white_redflag = QLabel('   ')
        self.__white_redflag.setAutoFillBackground(True)
        self.__white_blueflag = QLabel('   ')
        self.__white_blueflag.setAutoFillBackground(True)

        self.__black_greenflag = QLabel('   ')
        self.__black_greenflag.setAutoFillBackground(True)
        self.__black_yellowflag = QLabel('   ')
        self.__black_yellowflag.setAutoFillBackground(True)
        self.__black_redflag = QLabel('   ')
        self.__black_redflag.setAutoFillBackground(True)
        self.__black_blueflag = QLabel('   ')
        self.__black_blueflag.setAutoFillBackground(True)

    def changeDiceValue(self, dice):
        self.__dicecounter.setText(str(dice))

    def changePlayerStonesWhite(self, whitestones):
        self.__whitestonecounter.setText(str(whitestones))

    def changeFocusOnPlayerStones(self, isWhite):
        if isWhite:
            p = self.__whitestonecounter.palette()
            p.setColor(self.__white_greenflag.backgroundRole(), self.green)
            self.__whitestonecounter.setPalette(p)
            p = self.__blackstonecounter.palette()
            p.setColor(self.__white_greenflag.backgroundRole(), self.gray)
            self.__blackstonecounter.setPalette(p)
        else:
            p = self.__whitestonecounter.palette()
            p.setColor(self.__white_greenflag.backgroundRole(), self.gray)
            self.__whitestonecounter.setPalette(p)
            p = self.__blackstonecounter.palette()
            p.setColor(self.__white_greenflag.backgroundRole(), self.green)
            self.__blackstonecounter.setPalette(p)

    def changePlayerStonesBlack(self, blackstones):
        self.__blackstonecounter.setText(str(blackstones))

    def changeStatusWhiteGreen(self, green):
        if green:
            self.__white_greenflag.setText(str('EXT'))
            p = self.__white_greenflag.palette()
            p.setColor(self.__white_greenflag.backgroundRole(), self.green)
            self.__white_greenflag.setPalette(p)
        else:
            self.__white_greenflag.setText(str('   '))
            p = self.__white_greenflag.palette()
            p.setColor(self.__white_greenflag.backgroundRole(), self.gray)
            self.__white_greenflag.setPalette(p)

    def changeStatusWhiteYellow(self, yellow):
        if yellow:
            self.__white_yellowflag.setText(str('RND'))
            p = self.__white_yellowflag.palette()
            p.setColor(self.__white_yellowflag.backgroundRole(), self.yellow)
            self.__white_yellowflag.setPalette(p)
        else:
            self.__white_yellowflag.setText(str('  '))
            p = self.__white_yellowflag.palette()
            p.setColor(self.__white_yellowflag.backgroundRole(), self.gray)
            self.__white_yellowflag.setPalette(p)

    def changeStatusWhiteRed(self, red):
        if red:
            self.__white_redflag.setText(str('DEF'))
            p = self.__white_redflag.palette()
            p.setColor(self.__white_redflag.backgroundRole(), self.red)
            self.__white_redflag.setPalette(p)
        else:
            self.__white_redflag.setText(str('   '))
            p = self.__white_redflag.palette()
            p.setColor(self.__white_redflag.backgroundRole(), self.gray)
            self.__white_redflag.setPalette(p)

    def changeStatusWhiteBlue(self, blue):
        if blue:
            self.__white_blueflag.setText(str('REV'))
            p = self.__white_blueflag.palette()
            p.setColor(self.__white_blueflag.backgroundRole(), self.blue)
            self.__white_blueflag.setPalette(p)
        else:
            self.__white_blueflag.setText(str('  '))
            p = self.__white_blueflag.palette()
            p.setColor(self.__white_blueflag.backgroundRole(), self.gray)
            self.__white_blueflag.setPalette(p)

    def changeStatusBlackGreen(self, green):
        if green:
            self.__black_greenflag.setText(str('EXT'))
            p = self.__black_greenflag.palette()
            p.setColor(self.__black_greenflag.backgroundRole(), self.green)
            self.__black_greenflag.setPalette(p)
        else:
            self.__black_greenflag.setText(str('   '))
            p = self.__black_greenflag.palette()
            p.setColor(self.__black_greenflag.backgroundRole(), self.gray)
            self.__black_greenflag.setPalette(p)

    def changeStatusBlackYellow(self, yellow):
        if yellow:
            self.__black_yellowflag.setText(str('RND'))
            p = self.__black_yellowflag.palette()
            p.setColor(self.__black_yellowflag.backgroundRole(), self.yellow)
            self.__black_yellowflag.setPalette(p)
        else:
            self.__black_yellowflag.setText(str('  '))
            p = self.__black_yellowflag.palette()
            p.setColor(self.__black_yellowflag.backgroundRole(), self.gray)
            self.__black_yellowflag.setPalette(p)

    def changeStatusBlackRed(self, red):
        if red:
            self.__black_redflag.setText(str('DEF'))
            p = self.__black_redflag.palette()
            p.setColor(self.__black_redflag.backgroundRole(), self.red)
            self.__black_redflag.setPalette(p)
        else:
            self.__black_redflag.setText(str('   '))
            p = self.__black_redflag.palette()
            p.setColor(self.__black_redflag.backgroundRole(), self.gray)
            self.__black_redflag.setPalette(p)

    def changeStatusBlackBlue(self, blue):
        if blue:
            self.__black_blueflag.setText(str('REV'))
            p = self.__black_blueflag.palette()
            p.setColor(self.__black_blueflag.backgroundRole(), self.blue)
            self.__black_blueflag.setPalette(p)
        else:
            self.__black_blueflag.setText(str('  '))
            p = self.__black_blueflag.palette()
            p.setColor(self.__black_blueflag.backgroundRole(), self.gray)
            self.__black_blueflag.setPalette(p)

    def clearStatus(self):
        self.changeDiceValue(0)
        self.changePlayerStonesBlack(0)
        self.changePlayerStonesWhite(0)
        self.changeStatusBlackBlue(False)
        self.changeStatusBlackGreen(False)
        self.changeStatusBlackRed(False)
        self.changeStatusBlackYellow(False)
        self.changeStatusWhiteBlue(False)
        self.changeStatusWhiteGreen(False)
        self.changeStatusWhiteRed(False)
        self.changeStatusWhiteYellow(False)