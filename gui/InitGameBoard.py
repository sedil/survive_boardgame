from PyQt5.QtWidgets import QWidget
from PyQt5.QtGui import QPainter, QColor

from root.Board import Board
from root.Player import Player
from root.EventStone import EventStone

class InitGameBoard(QWidget):

    def __init__(self, mainframe, parent=None):
        super(InitGameBoard, self).__init__(parent)
        self.mainframe = mainframe
        self.board = Board()
        self.__whiteTurn = True
        self.__stones = 10 # 10
        self.__id_counter = 5
        self.__whitestones = []
        self.__blackstones = []
        self.__colorstones = []
        self.__colorstones.append(EventStone(1, 'red'))
        self.__colorstones[0].setPosition(0, 0)
        self.__colorstones.append(EventStone(2, 'green'))
        self.__colorstones[1].setPosition(0, 7)
        self.__colorstones.append(EventStone(3, 'yellow'))
        self.__colorstones[2].setPosition(7, 0)
        self.__colorstones.append(EventStone(4, 'blue'))
        self.__colorstones[3].setPosition(7, 7)

        self.__grid = []
        for x in range(8):
            self.__grid.append([])
            for y in range(8):
                self.__grid[x].append(True)
        self.__grid[0][0] = False
        self.__grid[0][7] = False
        self.__grid[7][0] = False
        self.__grid[7][7] = False

        self.__rectsize = 80
        self.setMouseTracking(True)
        self.setFixedSize(640, 640)
        self.__initColors()
        self.setGeometry(100, 100, 640, 640)
        self.setFixedSize(640, 640)
        self.setWindowTitle('Initialization')
        self.show()

    def __initColors(self):
        self.lightgray = QColor(190, 190, 190)
        self.darkgray = QColor(105, 105, 105)
        self.white = QColor(255, 255, 255)
        self.black = QColor(0, 0, 0)
        self.red = QColor(255, 0, 0)
        self.blue = QColor(0, 0, 255)
        self.green = QColor(0, 255, 0)
        self.yellow = QColor(255, 255, 0)

    def __placePlayerStones(self,cell_x,cell_y):

        if not self.__isOccupied(cell_x, cell_y):
            return

        if self.__stones > 0:
            if self.__whiteTurn:
                player = Player(self.__id_counter, True)
                player.setPosition(cell_x, cell_y)
                self.__whitestones.append(player)
                self.__whiteTurn = False
                self.__stones -= 1
                self.__id_counter += 1
            else:
                player = Player(self.__id_counter, False)
                player.setPosition(cell_x, cell_y)
                self.__blackstones.append(player)
                self.__whiteTurn = True
                self.__stones -= 1
                self.__id_counter += 1

    def __isOccupied(self, cell_x, cell_y):
        if self.__grid[cell_x][cell_y]:
            self.__grid[cell_x][cell_y] = False
            return True
        else:
            return False

    def paintEvent(self, event):
        painter = QPainter()
        painter.begin(self)
        self.__drawChessPattern(painter)
        self.__drawStones(painter)
        painter.end()

    def mousePressEvent(self, mouseevent):
        cell_x = int(mouseevent.x() / self.__rectsize)
        cell_y = int(mouseevent.y() / self.__rectsize)
        self.__placePlayerStones(cell_x,cell_y)
        self.update()

        if self.__stones == 0:
            self.board.getWhiteStoneList().extend(self.getWhitePlayerStones())
            self.board.getBlackStoneList().extend(self.getBlackPlayerStones())
            self.board.getColorStoneList().extend(self.getColorStones())

            for idx in range(5):
                whitestone = self.board.getWhiteStoneList()[idx]
                self.board.getGrid(whitestone.getPosition()[0], whitestone.getPosition()[1]).appendStone(whitestone)
                blackstone = self.board.getBlackStoneList()[idx]
                self.board.getGrid(blackstone.getPosition()[0], blackstone.getPosition()[1]).appendStone(blackstone)
            self.close()
            self.mainframe.ready()

    def __drawChessPattern(self, painter):
        painter.setPen(self.lightgray)
        painter.setBrush(self.lightgray)
        painter.drawRect(0, 0, 640, 640)

        painter.setBrush(self.darkgray)
        for i in range(8):
            for j in range(8):
                if i % 2 == 0 and j % 2 == 0:
                    painter.drawRect(i * self.__rectsize, j * self.__rectsize, self.__rectsize, self.__rectsize)
                elif not i % 2 == 0 and not j % 2 == 0:
                    painter.drawRect(i * self.__rectsize, j * self.__rectsize, self.__rectsize, self.__rectsize)

    def __drawStones(self, painter):
        painter.setPen(self.white)

        for elem in self.__whitestones:
            x = elem.getPosition()[0] * self.__rectsize
            y = elem.getPosition()[1] * self.__rectsize
            painter.setBrush(self.white)
            painter.drawEllipse(10 + x, 10 + y, 60, 60)

        painter.setPen(self.black)
        for elem in self.__blackstones:
            x = elem.getPosition()[0] * self.__rectsize
            y = elem.getPosition()[1] * self.__rectsize
            painter.setBrush(self.black)
            painter.drawEllipse(10 + x, 10 + y, 60, 60)

        painter.setPen(self.red)
        painter.setBrush(self.red)
        painter.drawEllipse(15, 15, 50, 50)

        painter.setPen(self.yellow)
        painter.setBrush(self.yellow)
        painter.drawEllipse(15, 15 + 7 * self.__rectsize, 50, 50)

        painter.setPen(self.green)
        painter.setBrush(self.green)
        painter.drawEllipse(15 + 7 * self.__rectsize, 15, 50, 50)

        painter.setPen(self.blue)
        painter.setBrush(self.blue)
        painter.drawEllipse(15 + 7 * self.__rectsize, 15 + 7 * self.__rectsize, 50, 50)

    def getWhitePlayerStones(self):
        return self.__whitestones

    def getBlackPlayerStones(self):
        return self.__blackstones

    def getColorStones(self):
        return self.__colorstones

    def getBoardLogic(self):
        return self.board