import sys
from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout

from gui.StatusPanel import StatusPanel
from gui.GameBoard import GameBoard
from gui.Dialog import ChooseDialog

from gui.InitGameBoard import InitGameBoard

class MainFrame(QWidget):

    def __init__(self, parent=None):
        super(MainFrame, self).__init__(parent)
        self.init = InitGameBoard(self)

    def __initLayout(self):
        self.__layout = QVBoxLayout()
        self.__layout.addWidget(self.gameboard)
        self.__layout.addWidget(self.status)

    def ready(self):
        self.status = StatusPanel()
        self.gameboard = GameBoard(self, self.init.getBoardLogic())
        self.__initLayout()
        self.setLayout(self.__layout)
        self.setWindowTitle('Survive!')
        self.show()

    def updateStatusPanel(self):
        logic = self.init.getBoardLogic()
        self.status.changeDiceValue(logic.getDice().getDiceValue())
        self.status.changePlayerStonesWhite(len(logic.getWhiteStoneList()))
        self.status.changePlayerStonesBlack(len(logic.getBlackStoneList()))
        self.status.changeFocusOnPlayerStones(logic.getPlayersTurn())

        if logic.getPlayersTurn():
            self.status.changeStatusWhiteGreen(logic.getGreenFlag())
            self.status.changeStatusWhiteYellow(logic.getYellowFlag())
            self.status.changeStatusWhiteRed(False)
            self.status.changeStatusWhiteBlue(False)
            self.status.changeStatusBlackGreen(False)
            self.status.changeStatusBlackYellow(False)
            self.status.changeStatusBlackRed(logic.getRedFlag())
            self.status.changeStatusBlackBlue(logic.getBlueFlag())
        else:
            self.status.changeStatusBlackGreen(logic.getGreenFlag())
            self.status.changeStatusBlackYellow(logic.getYellowFlag())
            self.status.changeStatusBlackRed(False)
            self.status.changeStatusBlackBlue(False)
            self.status.changeStatusWhiteGreen(False)
            self.status.changeStatusWhiteYellow(False)
            self.status.changeStatusWhiteRed(logic.getRedFlag())
            self.status.changeStatusWhiteBlue(logic.getBlueFlag())

        (end, isWhite) = logic.isGameOver()
        if end and isWhite:
            dialog = ChooseDialog(self)
            dialog.setInfoMessage('Player White wins')
            if dialog.exec_():
                self.close()
                MainFrame()
            else:
                self.close()
        elif end and not isWhite:
            dialog = ChooseDialog(self)
            dialog.setInfoMessage('Player Black wins')
            if dialog.exec_():
                self.close()
                MainFrame()
            else:
                self.close()

if __name__ == "__main__":
    app = QApplication(sys.argv)
    gui = MainFrame()
    sys.exit(app.exec_())