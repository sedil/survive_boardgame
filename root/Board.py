from root.Grid import Grid
from root.Dice import Dice
from root.MovePattern import MovePattern
from gui.Dialog import InfoDialog

import random

class Board:

    def __init__(self):
        self.__dice = Dice()
        self.__movepattern = MovePattern()
        self.__grid = []
        self.__beatenStones = []
        self.__whiteStones = []
        self.__blackStones = []
        self.__colorStones = []
        self.__whiteTurn = True
        self.__actualID = -1
        self.__actualStone = None
        self.__allowToChooseStone = True
        self.__greenflag = False
        self.__allowNewDiceForGreen = False
        self.__yellowflag = False
        self.__redflag = False
        self.__blueflag = False

        for x in range(8):
            self.__grid.append([])
            for y in range(8):
                self.__grid[x].append(Grid(x,y))

    def isGameOver(self):
        ''' Player Black wins '''
        if len(self.__whiteStones) == 0:
            return (True, False)
        elif len(self.__blackStones) == 0:
            return (True, True)
        return (False, None)

    ''' no same color on one field '''
    def examineSameColorConflict(self, stone, posx, posy):
        targetgrid = self.__grid[posx][posy]
        stonelist = targetgrid.getStoneList()

        if len(stonelist) == 0:
            return True
        else:
            for elem in stonelist:
                if stone.getColor() == elem.getColor():
                    return False
            return True

    ''' examine if enemy stone can be beaten'''
    def canStoneBeaten(self, stone, posx, posy):
        targetgrid = self.__grid[posx][posy]
        stonelist = targetgrid.getStoneList()

        ''' field is empty '''
        if len(stonelist) == 0:
            return False
        else:
            for elem in stonelist:
                ''' beat one opponent '''
                if stone.getColor() != elem.getColor() and elem.isPlayer():
                    return True

    def moveStoneToPosition(self, stone, new_posx, new_posy):

        if self.examineSameColorConflict(stone, new_posx, new_posy):
            self.moveEventStones()

            if self.__dice.getDiceValue() == 1:
                allowToBeat = self.canStoneBeaten(stone, new_posx, new_posy)
                if allowToBeat:
                    ''' Grid has an enemystone. Beat him and collect him '''
                    opponentStone = self.getGrid(new_posx, new_posy).popStone()
                    self.__beatenStones.append(opponentStone)

                    if opponentStone.getColor():
                        for idx in range(len(self.getWhiteStoneList())):
                            if self.getWhiteStoneList()[idx].getStoneID() == opponentStone.getStoneID():
                                self.getWhiteStoneList().pop(idx)
                                print(len(self.getWhiteStoneList()))
                                break
                    else:
                        for idx in range(len(self.getBlackStoneList())):
                            if self.getBlackStoneList()[idx].getStoneID() == opponentStone.getStoneID():
                                self.getBlackStoneList().pop(idx)
                                print(len(self.getBlackStoneList()))
                                break

                    self.__grid[stone.getPosition()[0]][stone.getPosition()[1]].popStone()
                    self.getGrid(new_posx, new_posy).appendStone(stone)
                    stone.setPosition(new_posx, new_posy)
                    self.__dice.decrementDiceValue()

                    self.eventRed()
                    self.eventBlue()
                    self.eventYellow()
                    self.eventGreen()

                    if not self.__greenflag:
                        self.switchTurn()

                    if not self.__yellowflag:
                        self.__dice.newDice()

                    self.setChooseStoneFlag(True)
                else:
                    ''' Grid is empty. Place the stone '''
                    self.__grid[stone.getPosition()[0]][stone.getPosition()[1]].popStone()
                    self.getGrid(new_posx, new_posy).appendStone(stone)
                    stone.setPosition(new_posx, new_posy)
                    self.__dice.decrementDiceValue()

                    self.eventRed()
                    self.eventBlue()
                    self.eventYellow()
                    self.eventGreen()

                    if not self.__greenflag:
                        self.switchTurn()

                    if not self.__yellowflag:
                        self.__dice.newDice()

                    self.setChooseStoneFlag(True)
                    self.isGameOver()
            else:
                self.__grid[stone.getPosition()[0]][stone.getPosition()[1]].popStone()
                self.__grid[new_posx][new_posy].appendStone(stone)
                stone.setPosition(new_posx, new_posy)

                if self.__yellowflag:
                    self.__yellowflag = False

                self.__dice.decrementDiceValue()
                self.__redflag = False
                self.__blueflag = False
        else:
            dialog = InfoDialog()
            dialog.setInfoMessage('Move not allowed. Target position is occupied with your color')
            dialog.exec_()

            self.__redflag = False
            self.__blueflag = False

    def moveEventStones(self):
        self.__movepattern.moveRedStone(self.__colorStones[0])
        self.__movepattern.moveGreenStone(self.__colorStones[1])
        self.__movepattern.moveYellowStone(self.__colorStones[2])
        self.__movepattern.moveBlueStone(self.__colorStones[3])

    ''' red event: Kill any playerstone on the field '''
    def eventRed(self):
        posx = self.getColorStoneList()[0].getPosition()[0]
        posy = self.getColorStoneList()[0].getPosition()[1]

        if len(self.getGrid(posx, posy).getStoneList()) > 0:

            gridstones = self.getGrid(posx,posy).getStoneList()
            for i in range(len(gridstones)):
                for j in range(len(self.getWhiteStoneList())):
                    if gridstones[i].getStoneID() == self.getWhiteStoneList()[j].getStoneID():
                        self.getWhiteStoneList().pop(j)
                        break
                for k in range(len(self.getBlackStoneList())):
                    if gridstones[i].getStoneID() == self.getBlackStoneList()[k].getStoneID():
                        self.getBlackStoneList().pop(k)
                        break

            self.getBeatenStoneList().extend(gridstones)
            self.getGrid(posx,posy).getStoneList().clear()
            self.__redflag = True
            dialog = InfoDialog()
            dialog.setInfoMessage('Red Stone: One Stone was killed on Position (' + str(posx) + ',' + str(posy) + ')')
            dialog.exec_()
            print('some stones are defeated because of RED')

    ''' yellow event: Dice value is halfed or doubled '''
    def eventYellow(self):

        posx = self.getColorStoneList()[1].getPosition()[0]
        posy = self.getColorStoneList()[1].getPosition()[1]

        if len(self.getGrid(posx, posy).getStoneList()) > 0:

            if self.__yellowflag:
                self.__yellowflag = False
            else:
                self.__yellowflag = True
                event = random.randint(1, 2)
                if event == 1:
                    self.__dice.newDice(1, 3)
                    dialog = InfoDialog()
                    dialog.setInfoMessage('Yellow Stone: Activated the Slow-Dice')
                    dialog.exec_()
                elif event == 2:
                    self.__dice.newDice(7, 12)
                    dialog = InfoDialog()
                    dialog.setInfoMessage('Yellow Stone: Activated the Tempo-Dice')
                    dialog.exec_()
                else:
                    pass

    ''' green event: Players turn twice '''
    def eventGreen(self):

        posx = self.getColorStoneList()[2].getPosition()[0]
        posy = self.getColorStoneList()[2].getPosition()[1]

        if len(self.getGrid(posx, posy).getStoneList()) > 0 and \
                self.getGrid(posx, posy).getStoneList()[0].getColor() == self.__whiteTurn:

            if not self.__greenflag:
                self.__greenflag = True
                self.__allowNewDiceForGreen = True
                dialog = InfoDialog()
                dialog.setInfoMessage('Green Stone: Your turn again')
                dialog.exec_()
            else:
                self.__greenflag = False
                self.__allowNewDiceForGreen = False
        else:
            if self.__greenflag:
                self.__greenflag = False
                self.__allowNewDiceForGreen = False

    ''' revive one playerstone to a specific position (NOT IMPLEMENTET YET!) '''
    def eventBlue(self):

        posx = self.getColorStoneList()[3].getPosition()[0]
        posy = self.getColorStoneList()[3].getPosition()[1]
        grid = None

        if len(self.getGrid(posx, posy).getStoneList()) > 0 and \
                self.getGrid(posx, posy).getStoneList()[0].getColor() == self.__whiteTurn:

            ''' first empty position '''
            for x in range(8):
                for y in range(8):
                    if self.getGrid(x,y).isEmpty():
                        grid = self.getGrid(x,y)
                        break
                break

            print(grid.getGridCoordinate())

            ''' take first stone with the right color  '''
            for idx in range(len(self.getBeatenStoneList())):
                if self.getBeatenStoneList()[idx].getColor() == self.__whiteTurn:
                    self.__blueflag = True
                    revivedStone = self.getBeatenStoneList().pop(idx)
                    revivedStone.setPosition(grid.getGridCoordinate()[0],grid.getGridCoordinate()[1])
                    grid.appendStone(revivedStone)

                    if revivedStone.getColor():
                        self.getWhiteStoneList().append(revivedStone)
                        dialog = InfoDialog()
                        dialog.setInfoMessage('Blue Stone: Revived one White Stone')
                        dialog.exec_()
                    else:
                        self.getBlackStoneList().append(revivedStone)
                        dialog = InfoDialog()
                        dialog.setInfoMessage('Blue Stone: Revived one Black Stone')
                        dialog.exec_()
                    break

            self.__blueflag = True

    ''' after round end: switch turn to black or white'''
    def switchTurn(self):
        if self.__whiteTurn:
            self.__whiteTurn = False
        else:
            self.__whiteTurn = True

    ''' only to roundbegin: choose stone to move'''
    def choosePlayerStone(self, posx, posy):

        ''' player whites turn '''
        if self.getPlayersTurn():
            choosen_grid = self.getGrid(posx,posy)

            ''' Empty cell choosed '''
            if choosen_grid.isEmpty():
                self.__actualID = -1
            else:
                for elem in choosen_grid.getStoneList():
                    if elem.getColor() == self.getPlayersTurn() and elem.isPlayer():
                        self.__actualID = elem.getStoneID()
                        self.__actualStone = elem

                        ''' do not change stone until round end '''
                        self.setChooseStoneFlag(False)
                        return
                self.__actualID = -1
        else:
            choosen_grid = self.getGrid(posx,posy)
            if choosen_grid.isEmpty():
                self.__actualID= -1
            else:
                for elem in choosen_grid.getStoneList():
                    if elem.getColor() == self.getPlayersTurn() and elem.isPlayer():
                        self.__actualID = elem.getStoneID()
                        self.__actualStone = elem

                        ''' do not change stone until round end '''
                        self.setChooseStoneFlag(False)
                        print('BLACK ' + str(elem.getColor()) + ' ' + str(self.__actualID))
                        return
                self.__actualID = -1

    def roundloop(self, posx, posy, newposition):

        if self.__actualID == -1:
            self.choosePlayerStone(posx,posy)

            if self.__actualID == -1:
                return False
            else:
                if newposition[0] == self.__actualStone.getPosition()[0] and newposition[1] == self.__actualStone.getPosition()[1]:
                    return False

                self.moveStoneToPosition(self.__actualStone, newposition[0], newposition[1])
                return True
        else:
            ''' Control only the same stone '''
            if posx == self.__actualStone.getPosition()[0] and posy == self.__actualStone.getPosition()[1]:

                if newposition[0] == self.__actualStone.getPosition()[0] and newposition[1] == \
                        self.__actualStone.getPosition()[1]:
                    return False

                self.moveStoneToPosition(self.__actualStone, newposition[0], newposition[1])
                return True
            else:
                return False

    ''' examine if player want to switch his stone to another'''
    def doNotChangeStone(self, stone):
        if stone.getStoneID() != self.__actualID:
            return False
        return True

    ''' only on roundend choose to true'''
    def setChooseStoneFlag(self, choose):
        if choose:
            self.__allowToChooseStone = choose
            self.__actualID = -1
            self.__actualStone = None
        else:
            self.__allowToChooseStone = choose

    ''' flag if player is allowed to choose a stone in round begin '''
    def getChooseStoneFlag(self):
        return self.__allowToChooseStone

    ''' players turn : true = white '''
    def getPlayersTurn(self):
        return self.__whiteTurn

    def getActualStone(self):
        return self.__actualStone

    ''' get beatenstone list '''
    def getBeatenStoneList(self):
        return self.__beatenStones

    def getWhiteStoneList(self):
        return self.__whiteStones

    def getBlackStoneList(self):
        return self.__blackStones

    def getColorStoneList(self):
        return self.__colorStones

    def getGrid(self, posx, posy):
        return self.__grid[posx][posy]

    def getGreenFlag(self):
        return self.__greenflag

    def getYellowFlag(self):
        return self.__yellowflag

    def getRedFlag(self):
        return self.__redflag

    def getBlueFlag(self):
        return self.__blueflag

    def getDice(self):
        return self.__dice