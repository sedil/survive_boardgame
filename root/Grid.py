class Grid:

    def __init__(self, posx, posy):
        if posx >= 0 and posx <= 7 and posy >= 0 and posy <= 7:
            self.__stonelist = []
            self.__posx = posx
            self.__posy = posy

    def getGridCoordinate(self):
        return (self.__posx,self.__posy)

    def appendStone(self, stone):
        self.__stonelist.append(stone)

    def popStone(self):
        if len(self.__stonelist) > 0:
            return self.__stonelist.pop()

    def isEmpty(self):
        if len(self.__stonelist) == 0:
            return True
        return False

    def getStoneList(self):
        return self.__stonelist