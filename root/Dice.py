import random

''' Simulates a dice which can have values between 1 and 6 '''
class Dice:

    def __init__(self):
        self.__dicevalue = random.randint(1, 6)

    def newDice(self, start=None, end=None):
        if not start == None and not end == None:
            self.__dicevalue = random.randint(start, end)
        else:
            self.__dicevalue = random.randint(1, 6)

    def decrementDiceValue(self):
        if self.__dicevalue > 0:
            self.__dicevalue -= 1
        else:
            self.newDice()

    def getDiceValue(self):
        return self.__dicevalue