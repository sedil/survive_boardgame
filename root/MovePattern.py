''' This class defines the movepattern of the eventstones '''
class MovePattern:

    def __init__(self):
        pass

    def moveRedStone(self, red):
        x = red.getPosition()[0]
        y = red.getPosition()[1]

        if x % 2 == 0 and y < 7:
            red.setPosition(x,y + 1)
        elif x % 2 == 0 and y == 7:
            red.setPosition(x + 1, y)
        elif not x % 2 == 0 and x < 7 and y > 0:
            red.setPosition(x, y - 1)
        elif not x % 2 == 0 and x < 7 and y == 0:
            red.setPosition(x + 1,y)
        elif not x % 2 == 0 and x == 7 and y > 0:
            red.setPosition(x, y - 1)
        elif not x % 2 == 0 and x == 7 and y == 0:
            red.setPosition(0, 0)

    def moveYellowStone(self, yellow):
        x = yellow.getPosition()[0]
        y = yellow.getPosition()[1]

        if y % 2 == 0 and x > 0 and y >= 0:
            yellow.setPosition(x - 1, y)
        elif y % 2 == 0 and x == 0 and y >= 0:
            yellow.setPosition(x, y + 1)
        elif not y % 2 == 0 and x < 7 and y >= 0:
            yellow.setPosition(x + 1, y)
        elif not y % 2 == 0 and x == 7 and y < 7:
            yellow.setPosition(x, y + 1)
        elif not y % 2 == 0 and x == 7 and y == 7:
            yellow.setPosition(7, 0)

    def moveGreenStone(self, green):
        x = green.getPosition()[0]
        y = green.getPosition()[1]

        if not y % 2 == 0 and x < 7 and y <= 7:
            green.setPosition(x + 1, y)
        elif not y % 2 == 0 and x == 7 and y <= 7:
            green.setPosition(x, y - 1)
        elif y % 2 == 0 and x > 0 and y >= 0:
            green.setPosition(x - 1, y)
        elif y % 2 == 0 and x == 0 and y > 0:
            green.setPosition(x, y - 1)
        elif y % 2 == 0 and x == 0 and y == 0:
            green.setPosition(0, 7)

    def moveBlueStone(self, blue):
        x = blue.getPosition()[0]
        y = blue.getPosition()[1]

        if not x % 2 == 0 and x == 7 and y > 0:
            blue.setPosition(x, y - 1)
        if not x % 2 == 0 and x == 7 and y == 0:
            blue.setPosition(x - 1, y)
        if not x % 2 == 0 and x < 7 and y > 0:
            blue.setPosition(x,y - 1)
        if not x % 2 == 0 and x < 7 and y == 0:
            blue.setPosition(x - 1, y)
        if x % 2 == 0 and x >= 0 and y < 7:
            blue.setPosition(x,y + 1)
        if x % 2 == 0 and x > 0 and y == 7:
            blue.setPosition(x - 1, y)
        if x % 2 == 0 and x == 0 and y == 7:
            blue.setPosition(7, 7)
