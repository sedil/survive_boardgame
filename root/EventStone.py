from root.Stone import Stone

''' This class defines the eventstones with the colors: red, yellow, green and blue'''
class EventStone(Stone):

    def __init__(self, id, color):
        Stone.__init__(self,id)
        self.__color = color

    def getColor(self):
        return self.__color

    def isPlayer(self):
        return False

    def __repr__(self):
        return str(self.getStoneID()) + ' ' + str(self.getColor()) + ' ' + str(self.getPosition()[0]) + ' ' + str(self.getPosition()[1])