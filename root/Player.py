from root.Stone import Stone

''' Playerstone class '''
class Player(Stone):

    def __init__(self, id, isWhite):
        Stone.__init__(self, id)
        self.__isWhite = isWhite

    def getColor(self):
        return self.__isWhite

    def isPlayer(self):
        return True

    def __repr__(self):
        return str(self.getStoneID()) + ' ' + str(self.getColor()) + ' ' + str(self.getPosition()[0]) + ' ' + str(self.getPosition()[1])