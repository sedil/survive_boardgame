from abc import ABCMeta, abstractmethod

class Stone:
    __metaclass__ = ABCMeta

    def __init__(self,id):
        self.__ID = id
        self.__posx = -1
        self.__posy = -1

    def setPosition(self, x, y):
        if x >= 0 and x <= 7 and y >= 0 and y <= 7:
            self.__posx = x
            self.__posy = y

    @abstractmethod
    def getColor(self):
        pass

    @abstractmethod
    def isPlayer(self):
        pass

    def getStoneID(self):
        return self.__ID

    def getPosition(self):
        return (self.__posx,self.__posy)